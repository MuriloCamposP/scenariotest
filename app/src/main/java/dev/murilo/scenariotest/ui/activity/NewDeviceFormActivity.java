package dev.murilo.scenariotest.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import dev.murilo.scenariotest.R;
import dev.murilo.scenariotest.model.Device;
import dev.murilo.scenariotest.model.ENumScenarioDevice;

public class NewDeviceFormActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String APPBAR_TITLE_ADD_DEVICE = "Adicionar dispositivo";

    private EditText deviceName;
    private EditText deviceManufacturer;
    private Device newDevice;
    private int roomId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_device_form);
        setTitle(APPBAR_TITLE_ADD_DEVICE);

        getFieldsReference();

        createNewDevice();

        getRoomData();

        configureSpinner();
    }

    private void getFieldsReference() {
        deviceName = findViewById(R.id.device_name_edit_text);
        deviceManufacturer = findViewById(R.id.device_manufacturer_edit_text);
    }

    private void createNewDevice() {
        newDevice = new Device("Iluminação", ENumScenarioDevice.MODULO_ILUMINACAO);
        newDevice.setDeviceManufacturer("Scenario");
    }

    private void configureSpinner() {
        Spinner spinner = findViewById(R.id.spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.scenario_devices_array, android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    private void getRoomData() {
        Intent data = getIntent();

        if (data.hasExtra("roomId")) {
            roomId = data.getIntExtra("roomId", -1);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_add) {
            fillNewDeviceInformation();

            setResponse();

            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setResponse() {
        Intent insertResult = new Intent();
        insertResult.putExtra("device", newDevice);
        setResult(Activity.RESULT_OK, insertResult);
    }

    private void fillNewDeviceInformation() {
        String dName = deviceName.getText().toString();
        String dManufacturer = deviceManufacturer.getText().toString();
        newDevice.setRoomId(roomId);

        if (dName.isEmpty() && dManufacturer.isEmpty()) {
            switch (newDevice.getName()) {
                case "Iluminação":
                    newDevice.setKindOfDevice(ENumScenarioDevice.MODULO_ILUMINACAO);
                    break;
                case "Teclados":
                    newDevice.setKindOfDevice(ENumScenarioDevice.TECLADOS);
                    break;
                case "Controle de Cortina":
                    newDevice.setKindOfDevice(ENumScenarioDevice.MODULO_CONTROLE_CORTINA);
                    break;
                default:
            }
            newDevice.setDeviceManufacturer("Scenario");
        } else {
            newDevice.setName(dName);
            newDevice.setDeviceManufacturer(dManufacturer);
            newDevice.setKindOfDevice(ENumScenarioDevice.MODULO_ILUMINACAO);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        newDevice.setName(parent.getItemAtPosition(position).toString());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
