package dev.murilo.scenariotest.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import dev.murilo.scenariotest.R;
import dev.murilo.scenariotest.model.ProjectRoom;

public class NewProjectRoomFormActivity extends AppCompatActivity {

    private static final String APPBAR_TITLE_ADD_ROOM = "Adicionar ambiente";
    public static final int REQUEST_CAMERA_PERMISSION_CODE = 1003;
    public static final int REQUEST_STORAGE_PERMISSION_CODE = 1000;
    public static final int OPEN_GALLERY_REQUEST_CODE = 1001;
    public static final int OPEN_CAMERA_REQUEST_CODE = 1002;
    private EditText roomName;
    private Button loadPictureButton;
    private Button takePictureButton;
    private ImageView roomImagePreview;
    private ProjectRoom projectRoom;
    private Uri pictureFileUri = null;
    private int projectId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_project_room_form);
        setTitle(APPBAR_TITLE_ADD_ROOM);

        projectRoom = new ProjectRoom();

        getFieldsReference();

        getIntentData();

        configureLoadPictureButton();

        configureTakePictureButton();
    }

    private void configureTakePictureButton() {
        takePictureButton.setOnClickListener(v -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(NewProjectRoomFormActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    // Permission is not granted
                    requestPermissions(permission, REQUEST_CAMERA_PERMISSION_CODE);
                } else {
                    openCamera();
                }
            } else {
                openCamera();
            }
        });
    }

    private void configureLoadPictureButton() {
        loadPictureButton.setOnClickListener(v -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(NewProjectRoomFormActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    String[] permission = {Manifest.permission.READ_EXTERNAL_STORAGE};
                    // Permission is not granted
                    requestPermissions(permission, REQUEST_STORAGE_PERMISSION_CODE);
                } else {
                    openGallery();
                }
            } else {
                openGallery();
            }
        });
    }

    private void getIntentData() {
        Intent data = getIntent();

        if (data.hasExtra("projectId")) {
            projectId = data.getIntExtra("projectId", -1);
        }
    }

    private void getFieldsReference() {
        roomName = findViewById(R.id.activity_new_project_room_form_room_name);
        loadPictureButton = findViewById(R.id.button_load_picture);
        takePictureButton = findViewById(R.id.button_take_picture);
        roomImagePreview = findViewById(R.id.room_image_preview);
    }

    private void openCamera() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Making sure there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Creating File
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error
                Toast.makeText(this, "Error while creating File", Toast.LENGTH_SHORT).show();
            }
            // Continue if the File was created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "dev.murilo.scenariotest.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, OPEN_CAMERA_REQUEST_CODE);
            }
        }
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, OPEN_GALLERY_REQUEST_CODE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_add) {

            if (pictureFileUri == null) {
                new AlertDialog.Builder(this)
                        .setTitle("Imagem requerida")
                        .setMessage("Por favor escolha uma imagem para o ambiente")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            } else {
                fillNewRoomInformation();
                setResponse();
                finish();
            }


        }
        return super.onOptionsItemSelected(item);
    }

    private void setResponse() {
        Intent insertResult = new Intent();
        insertResult.putExtra("projectRoom", projectRoom);
        setResult(Activity.RESULT_OK, insertResult);
    }

    private void fillNewRoomInformation() {
        projectRoom.setName(roomName.getText().toString());
        projectRoom.setPicturePath(pictureFileUri.toString());
        projectRoom.setProjectId(projectId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Picasso.get().load(pictureFileUri).fit().centerCrop().into(roomImagePreview);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK &&
                requestCode == OPEN_GALLERY_REQUEST_CODE && data != null) {
            pictureFileUri = data.getData();
            final int takeFlags = data.getFlags()
                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            getContentResolver().takePersistableUriPermission(pictureFileUri, takeFlags);
            Picasso.get().load(pictureFileUri).fit().centerCrop().into(roomImagePreview);
        }

        if (resultCode == Activity.RESULT_OK &&
                requestCode == OPEN_CAMERA_REQUEST_CODE && data != null) {
            pictureFileUri = data.getData();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        pictureFileUri = Uri.fromFile(image);

        return image;
    }
}
