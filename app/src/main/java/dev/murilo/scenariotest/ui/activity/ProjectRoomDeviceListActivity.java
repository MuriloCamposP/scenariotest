package dev.murilo.scenariotest.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.ExecutionException;

import dev.murilo.scenariotest.R;
import dev.murilo.scenariotest.asynctask.AddDeviceTask;
import dev.murilo.scenariotest.asynctask.GetDevicesTask;
import dev.murilo.scenariotest.database.ScenarioDatabase;
import dev.murilo.scenariotest.database.dao.DeviceDAO;
import dev.murilo.scenariotest.model.Device;
import dev.murilo.scenariotest.model.ProjectRoom;
import dev.murilo.scenariotest.ui.recyclerview.adapter.DeviceListAdapter;
import dev.murilo.scenariotest.ui.recyclerview.helper.DeviceItemTouchHelperCallback;

public class ProjectRoomDeviceListActivity extends AppCompatActivity {

    public static final String APPBAR_TITLE = "Dispositivos";
    public static final int OPEN_DEVICE_FORM_REQUEST_CODE = 1005;
    private ProjectRoom room;
    private ImageView roomImage;
    private DeviceListAdapter deviceListAdapter;
    private DeviceDAO dao;
    private List<Device> devices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_room_device_list);
        roomImage = findViewById(R.id.room_image);

        dao = ScenarioDatabase.getInstance(this).getDeviceDAO();

        getIntentData();

        setTitle(room.getName() + " / " + APPBAR_TITLE);

        getDevices();

        Uri imageUri = Uri.parse(room.getPicturePath());
        Picasso.get().load(imageUri).placeholder(R.drawable.loading_image_placeholder).error(R.drawable.error_loading_image).fit().centerCrop().into(roomImage);

        configureNewDeviceButton();

        configureRecyclerView();
    }

    private void configureRecyclerView() {
        RecyclerView devicesList = findViewById(R.id.project_room_device_list_recycler_view);
        deviceListAdapter = new DeviceListAdapter(devices, this);
        devicesList.setAdapter(deviceListAdapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new DeviceItemTouchHelperCallback(deviceListAdapter, dao));
        itemTouchHelper.attachToRecyclerView(devicesList);
    }

    private void configureNewDeviceButton() {
        FloatingActionButton newDeviceButton = findViewById(R.id.new_device_fab);
        newDeviceButton.setOnClickListener(v -> {
            Intent startNewDeviceForm = new Intent(ProjectRoomDeviceListActivity.this, NewDeviceFormActivity.class);
            startNewDeviceForm.putExtra("roomId", room.getId());
            startActivityForResult(startNewDeviceForm, OPEN_DEVICE_FORM_REQUEST_CODE);
        });
    }

    private void getIntentData() {
        Intent data = getIntent();

        if (data.hasExtra("room")) {
            room = data.getExtras().getParcelable("room");
        }
    }

    private void getDevices() {
        try {
            devices = new GetDevicesTask(dao, room.getId()).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_info) {
            Intent openInfoActivity = new Intent(ProjectRoomDeviceListActivity.this, InfoActivity.class);
            startActivity(openInfoActivity);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK
                && requestCode == OPEN_DEVICE_FORM_REQUEST_CODE
                && data != null
                && data.hasExtra("device")) {
            Device newDevice = (Device) data.getSerializableExtra("device");
            new AddDeviceTask(dao, newDevice).execute();
            deviceListAdapter.add(newDevice);
        }
    }
}
