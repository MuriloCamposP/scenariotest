package dev.murilo.scenariotest.ui.recyclerview.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import dev.murilo.scenariotest.R;
import dev.murilo.scenariotest.model.Project;
import dev.murilo.scenariotest.ui.recyclerview.adapter.listener.OnItemClickListener;

public class ProjectListAdapter extends RecyclerView.Adapter<ProjectListAdapter.ProjectViewHolder> {

    private final List<Project> projects;
    private final Context context;
    private OnItemClickListener onItemClickListener;

    public ProjectListAdapter(Context context, List<Project> projects) {
        this.context = context;
        this.projects = projects;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ProjectListAdapter.ProjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View createdView = LayoutInflater.from(context)
                .inflate(R.layout.item_project, parent, false);
        return new ProjectViewHolder(createdView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectListAdapter.ProjectViewHolder holder, int position) {
        Project project = projects.get(position);
        holder.bind(project);
    }

    @Override
    public int getItemCount() {
        return projects.size();
    }

    class ProjectViewHolder extends RecyclerView.ViewHolder {

        private final TextView title;
        private final TextView address;
        private Project project;

        public ProjectViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_project_title);
            address = itemView.findViewById(R.id.item_project_address);

            itemView.setOnClickListener(v -> onItemClickListener.onProjectClick(project, getAdapterPosition()));
        }

        public void bind(Project project) {
            this.project = project;
            title.setText(project.getProjectName());
            address.setText(project.getAddress());
        }
    }

    public void add(Project project) {
        projects.add(project);
        notifyDataSetChanged();
    }

    public Project getProject(int position) {
        return projects.get(position);
    }

    public void remove(int position) {
        projects.remove(position);
        notifyItemRemoved(position);
    }
}
