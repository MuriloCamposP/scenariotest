package dev.murilo.scenariotest.ui.recyclerview.adapter.listener;

import dev.murilo.scenariotest.model.Project;
import dev.murilo.scenariotest.model.ProjectRoom;

public interface OnItemClickListener {

    void onProjectClick(Project project, int position);
    void onRoomClick(ProjectRoom room, int position);
}
