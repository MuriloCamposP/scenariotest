package dev.murilo.scenariotest.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;
import java.util.concurrent.ExecutionException;

import dev.murilo.scenariotest.R;
import dev.murilo.scenariotest.asynctask.AddProjectRoomTask;
import dev.murilo.scenariotest.asynctask.GetProjectRoomsTask;
import dev.murilo.scenariotest.database.ScenarioDatabase;
import dev.murilo.scenariotest.database.dao.ProjectRoomDAO;
import dev.murilo.scenariotest.model.Project;
import dev.murilo.scenariotest.model.ProjectRoom;
import dev.murilo.scenariotest.ui.recyclerview.adapter.ProjectRoomListAdapter;
import dev.murilo.scenariotest.ui.recyclerview.adapter.listener.OnItemClickListener;
import dev.murilo.scenariotest.ui.recyclerview.helper.RoomItemTouchHelperCallback;

public class ProjectRoomListActivity extends AppCompatActivity {

    public static final String APPBAR_TITLE = "Ambientes";
    public static final int OPEN_ROOM_FORM_REQUEST_CODE = 1005;
    private ProjectRoomListAdapter adapter;
    private List<ProjectRoom> projectRooms;
    private int projectId;
    private ProjectRoomDAO projectRoomDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_room_list);
        setTitle(APPBAR_TITLE);

        projectRoomDAO = ScenarioDatabase.getInstance(this).getProjectRoomDAO();

        configureAddRoomButton();

        getProjectData();

        getProjectRooms();

        configureRecyclerView();
    }

    private void configureRecyclerView() {
        RecyclerView projectRoomList = findViewById(R.id.project_room_list_recycler_view);
        adapter = new ProjectRoomListAdapter(projectRooms, this);
        projectRoomList.setAdapter(adapter);
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onProjectClick(Project project, int position) {
            }

            @Override
            public void onRoomClick(ProjectRoom room, int position) {
                Intent openProjectRoomDeviceList = new Intent(ProjectRoomListActivity.this, ProjectRoomDeviceListActivity.class);
                openProjectRoomDeviceList.putExtra("room", room);
                startActivity(openProjectRoomDeviceList);
            }
        });

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new RoomItemTouchHelperCallback(adapter, projectRoomDAO));
        itemTouchHelper.attachToRecyclerView(projectRoomList);
    }

    private void getProjectRooms() {
        if (projectId > -1) {
            try {
                projectRooms = new GetProjectRoomsTask(projectRoomDAO, projectId).execute().get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void getProjectData() {
        Intent data = getIntent();

        if (data.hasExtra("projectId")) {
            projectId = data.getIntExtra("projectId", -1);
        }
    }

    private void configureAddRoomButton() {
        FloatingActionButton newRoomButton = findViewById(R.id.new_room_fab);
        newRoomButton.setOnClickListener(v -> {
            Intent startNewRoomForm = new Intent(ProjectRoomListActivity.this, NewProjectRoomFormActivity.class);
            startNewRoomForm.putExtra("projectId", projectId);

            startActivityForResult(startNewRoomForm, OPEN_ROOM_FORM_REQUEST_CODE);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == OPEN_ROOM_FORM_REQUEST_CODE && data != null) {
            ProjectRoom newProjectRoom = data.getExtras().getParcelable("projectRoom");
            try {
                int id = new AddProjectRoomTask(projectRoomDAO, newProjectRoom).execute().get().intValue();
                newProjectRoom.setId(id);
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            adapter.add(newProjectRoom);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_info) {
            Intent openInfoActivity = new Intent(ProjectRoomListActivity.this, InfoActivity.class);
            startActivity(openInfoActivity);
        }
        return super.onOptionsItemSelected(item);
    }
}
