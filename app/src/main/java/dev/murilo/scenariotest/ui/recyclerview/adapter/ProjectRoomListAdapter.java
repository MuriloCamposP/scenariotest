package dev.murilo.scenariotest.ui.recyclerview.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import dev.murilo.scenariotest.R;
import dev.murilo.scenariotest.model.ProjectRoom;
import dev.murilo.scenariotest.ui.recyclerview.adapter.listener.OnItemClickListener;

public class ProjectRoomListAdapter extends RecyclerView.Adapter<ProjectRoomListAdapter.ProjectRoomViewHolder> {

    private final List<ProjectRoom> rooms;
    private final Context context;
    private OnItemClickListener onItemClickListener;

    public ProjectRoomListAdapter(List<ProjectRoom> rooms, Context context) {
        this.rooms = rooms;
        this.context = context;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ProjectRoomListAdapter.ProjectRoomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View createdView = LayoutInflater.from(context)
                .inflate(R.layout.item_project_room, parent, false);
        return new ProjectRoomViewHolder(createdView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectRoomListAdapter.ProjectRoomViewHolder holder, int position) {
        ProjectRoom room = rooms.get(position);
        holder.bind(room);
    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }

    class ProjectRoomViewHolder extends RecyclerView.ViewHolder {

        private final ImageView roomImage;
        private final TextView roomName;
        private ProjectRoom room;

        public ProjectRoomViewHolder(@NonNull View itemView) {
            super(itemView);
            roomImage = itemView.findViewById(R.id.item_project_room_image);
            roomName = itemView.findViewById(R.id.item_project_room_roomname);

            itemView.setOnClickListener(v -> onItemClickListener.onRoomClick(room, getAdapterPosition()));
        }

        public void bind(ProjectRoom room) {
            this.room = room;
            Uri imageUri = Uri.parse(room.getPicturePath());
            Picasso.get().load(imageUri).placeholder(R.drawable.loading_image_placeholder).error(R.drawable.error_loading_image).fit().centerCrop().into(roomImage);
            roomName.setText(room.getName());
        }
    }

    public void add(ProjectRoom projectRoom) {
        rooms.add(projectRoom);
        notifyDataSetChanged();
    }

    public ProjectRoom getRoom(int position) {
        return rooms.get(position);
    }

    public void remove(int position) {
        rooms.remove(position);
        notifyItemRemoved(position);
    }


}
