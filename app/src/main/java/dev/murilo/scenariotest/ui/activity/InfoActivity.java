package dev.murilo.scenariotest.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import dev.murilo.scenariotest.R;

public class InfoActivity extends AppCompatActivity {

    public static final String APPBAR_TITLE = "Informações";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        setTitle(APPBAR_TITLE);

        ImageView imageView = findViewById(R.id.room_image);
        Picasso.get().load(R.drawable.scenario).placeholder(R.drawable.loading_image_placeholder).error(R.drawable.error_loading_image).fit().into(imageView);
    }
}
