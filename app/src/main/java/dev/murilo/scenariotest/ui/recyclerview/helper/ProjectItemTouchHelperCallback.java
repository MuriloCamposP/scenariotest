package dev.murilo.scenariotest.ui.recyclerview.helper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import dev.murilo.scenariotest.asynctask.DeleteProjectTask;
import dev.murilo.scenariotest.database.dao.ProjectDAO;
import dev.murilo.scenariotest.model.Project;
import dev.murilo.scenariotest.ui.recyclerview.adapter.ProjectListAdapter;

public class ProjectItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final ProjectListAdapter adapter;
    private final ProjectDAO dao;

    public ProjectItemTouchHelperCallback(ProjectListAdapter adapter, ProjectDAO dao) {
        this.adapter = adapter;
        this.dao = dao;
    }

    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        int swipeFlags = ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT;
        return makeMovementFlags(0, swipeFlags);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        int position = viewHolder.getAdapterPosition();
        Project projectToRemove = adapter.getProject(position);
        new DeleteProjectTask(dao, projectToRemove).execute();
        adapter.remove(position);
    }
}
