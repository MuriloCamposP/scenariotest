package dev.murilo.scenariotest.ui.recyclerview.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import dev.murilo.scenariotest.R;
import dev.murilo.scenariotest.model.Device;

public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.DeviceViewHolder> {

    private final List<Device> devices;
    private final Context context;

    public DeviceListAdapter(List<Device> devices, Context context) {
        this.devices = devices;
        this.context = context;
    }

    @NonNull
    @Override
    public DeviceListAdapter.DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View createdView = LayoutInflater.from(context)
                .inflate(R.layout.item_device, parent, false);
        return new DeviceViewHolder(createdView);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceListAdapter.DeviceViewHolder holder, int position) {
        Device device = devices.get(position);
        holder.bind(device);
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    class DeviceViewHolder extends RecyclerView.ViewHolder {

        private final TextView deviceKind;
        private final TextView deviceManufacturer;
        private Device device;

        public DeviceViewHolder(@NonNull View itemView) {
            super(itemView);

            deviceKind = itemView.findViewById(R.id.item_device_kind);
            deviceManufacturer = itemView.findViewById(R.id.item_device_manufacturer);
        }

        public void bind(Device device) {
            this.device = device;
            deviceKind.setText(device.getName());
            deviceManufacturer.setText(device.getDeviceManufacturer());
        }
    }

    public void add(Device device) {
        devices.add(device);
        notifyDataSetChanged();
    }

    public Device getDevice(int position) {
        return devices.get(position);
    }

    public void remove(int position) {
        devices.remove(position);
        notifyItemRemoved(position);
    }
}
