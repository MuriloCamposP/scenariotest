package dev.murilo.scenariotest.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;
import java.util.concurrent.ExecutionException;

import dev.murilo.scenariotest.R;
import dev.murilo.scenariotest.asynctask.AddProjectTask;
import dev.murilo.scenariotest.asynctask.GetProjectsTask;
import dev.murilo.scenariotest.database.ScenarioDatabase;
import dev.murilo.scenariotest.database.dao.ProjectDAO;
import dev.murilo.scenariotest.model.Project;
import dev.murilo.scenariotest.model.ProjectRoom;
import dev.murilo.scenariotest.ui.recyclerview.adapter.ProjectListAdapter;
import dev.murilo.scenariotest.ui.recyclerview.adapter.listener.OnItemClickListener;
import dev.murilo.scenariotest.ui.recyclerview.helper.ProjectItemTouchHelperCallback;

public class ProjectListActivity extends AppCompatActivity {

    public static final String APPBAR_TITLE = "Projetos";
    public static final int OPEN_PROJECT_FORM_REQUEST_CODE = 1;
    private ProjectListAdapter adapter;
    private List<Project> allProjects;
    private ProjectDAO projectDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_list);
        setTitle(APPBAR_TITLE);

        projectDAO = ScenarioDatabase.getInstance(this).getProjectDAO();

        FloatingActionButton newProjectButton = findViewById(R.id.new_project_fab);
        newProjectButton.setOnClickListener(v -> {
            Intent startNewProjectForm =
                    new Intent(ProjectListActivity.this, NewProjectFormActivity.class);
            startActivityForResult(startNewProjectForm, OPEN_PROJECT_FORM_REQUEST_CODE);
        });

        getProjects();

        configureRecyclerView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OPEN_PROJECT_FORM_REQUEST_CODE
                && data != null && data.hasExtra("project")) {
            if (resultCode == Activity.RESULT_OK) {
                Project newProject = (Project) data.getSerializableExtra("project");
                try {
                    int id = new AddProjectTask(projectDAO, newProject).execute().get().intValue();
                    newProject.setId(id);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                adapter.add(newProject);
            }
        }
    }

    private void configureRecyclerView() {
        RecyclerView projectList = findViewById(R.id.project_list_recycler_view);
        adapter = new ProjectListAdapter(this, allProjects);
        projectList.setAdapter(adapter);

        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onProjectClick(Project project, int position) {
                Intent openProjectRoomList = new Intent(ProjectListActivity.this,
                        ProjectRoomListActivity.class);
                openProjectRoomList.putExtra("projectId", project.getId());
                startActivity(openProjectRoomList);
            }

            @Override
            public void onRoomClick(ProjectRoom room, int position) {
            }
        });

        ItemTouchHelper itemTouchHelper =
                new ItemTouchHelper(new ProjectItemTouchHelperCallback(adapter, projectDAO));
        itemTouchHelper.attachToRecyclerView(projectList);
    }

    private void getProjects() {
        try {
            allProjects = new GetProjectsTask(projectDAO).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_info) {
            Intent openInfoActivity =
                    new Intent(ProjectListActivity.this, InfoActivity.class);
            startActivity(openInfoActivity);
        }
        return super.onOptionsItemSelected(item);
    }

}
