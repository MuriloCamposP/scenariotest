package dev.murilo.scenariotest.ui.recyclerview.helper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import dev.murilo.scenariotest.asynctask.DeleteRoomTask;
import dev.murilo.scenariotest.database.dao.ProjectRoomDAO;
import dev.murilo.scenariotest.model.ProjectRoom;
import dev.murilo.scenariotest.ui.recyclerview.adapter.ProjectRoomListAdapter;

public class RoomItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final ProjectRoomListAdapter adapter;
    private final ProjectRoomDAO dao;

    public RoomItemTouchHelperCallback(ProjectRoomListAdapter adapter, ProjectRoomDAO dao) {
        this.adapter = adapter;
        this.dao = dao;
    }

    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        int swipeFlags = ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT;
        return makeMovementFlags(0, swipeFlags);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        int position = viewHolder.getAdapterPosition();
        ProjectRoom roomToRemove = adapter.getRoom(position);
        new DeleteRoomTask(dao, roomToRemove).execute();
        adapter.remove(position);
    }
}
