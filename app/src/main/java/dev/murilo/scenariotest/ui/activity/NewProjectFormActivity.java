package dev.murilo.scenariotest.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import dev.murilo.scenariotest.R;
import dev.murilo.scenariotest.database.ScenarioDatabase;
import dev.murilo.scenariotest.database.dao.ProjectDAO;
import dev.murilo.scenariotest.model.Project;

public class NewProjectFormActivity extends AppCompatActivity {

    private static final String APPBAR_TITLE_ADD_PROJECT = "Criar projeto";
    private EditText projectName;
    private EditText clientName;
    private EditText address;
    private ProjectDAO projectDAO;
    private Project project;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_project_form);
        ScenarioDatabase database = ScenarioDatabase.getInstance(this);
        setTitle(APPBAR_TITLE_ADD_PROJECT);
        projectDAO = database.getProjectDAO();

        getFieldsReference();

        project = new Project();
    }

    private void getFieldsReference() {
        projectName = findViewById(R.id.activity_new_project_form_project_name);
        clientName = findViewById(R.id.activity_new_project_form_client_name);
        address = findViewById(R.id.activity_new_project_form_address);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_add) {
            fillNewProjectInformation();

            setResponse();

            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setResponse() {
        Intent insertResult = new Intent();
        insertResult.putExtra("project", project);
        setResult(Activity.RESULT_OK, insertResult);
    }

    private void fillNewProjectInformation() {
        String pName = projectName.getText().toString();
        String cName = clientName.getText().toString();
        String ad = address.getText().toString();

        project.setProjectName(pName);
        project.setClientName(cName);
        project.setAddress(ad);
    }
}
