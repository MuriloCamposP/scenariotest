package dev.murilo.scenariotest.ui.recyclerview.helper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import dev.murilo.scenariotest.asynctask.DeleteDeviceTask;
import dev.murilo.scenariotest.database.dao.DeviceDAO;
import dev.murilo.scenariotest.model.Device;
import dev.murilo.scenariotest.ui.recyclerview.adapter.DeviceListAdapter;

public class DeviceItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final DeviceListAdapter adapter;
    private final DeviceDAO dao;

    public DeviceItemTouchHelperCallback(DeviceListAdapter adapter, DeviceDAO dao) {
        this.adapter = adapter;
        this.dao = dao;
    }

    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        int swipeFlags = ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT;
        return makeMovementFlags(0, swipeFlags);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        int position = viewHolder.getAdapterPosition();
        Device deviceToRemove = adapter.getDevice(position);
        new DeleteDeviceTask(dao, deviceToRemove).execute();
        adapter.remove(position);
    }
}
