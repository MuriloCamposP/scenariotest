package dev.murilo.scenariotest.asynctask;

import android.os.AsyncTask;

import java.util.List;

import dev.murilo.scenariotest.database.dao.DeviceDAO;
import dev.murilo.scenariotest.model.Device;

public class GetDevicesTask extends AsyncTask<Void, Void, List<Device>> {

    private final DeviceDAO dao;
    private final int roomId;

    public GetDevicesTask(DeviceDAO dao, int roomId) {
        this.dao = dao;
        this.roomId = roomId;
    }

    @Override
    protected List<Device> doInBackground(Void... voids) {
        return dao.roomDevices(roomId);
    }
}
