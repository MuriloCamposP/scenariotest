package dev.murilo.scenariotest.asynctask;

import android.os.AsyncTask;

import dev.murilo.scenariotest.database.dao.ProjectRoomDAO;
import dev.murilo.scenariotest.model.ProjectRoom;

public class DeleteRoomTask extends AsyncTask<Void, Void, Void> {

    private final ProjectRoomDAO dao;
    private final ProjectRoom room;

    public DeleteRoomTask(ProjectRoomDAO dao, ProjectRoom room) {
        this.dao = dao;
        this.room = room;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        dao.remove(room);
        return null;
    }
}
