package dev.murilo.scenariotest.asynctask;

import android.os.AsyncTask;

import dev.murilo.scenariotest.database.dao.ProjectDAO;
import dev.murilo.scenariotest.model.Project;

public class AddProjectTask extends AsyncTask<Void, Void, Long> {

    private final ProjectDAO dao;
    private Project project;

    public AddProjectTask(ProjectDAO dao, Project project) {
        this.dao = dao;
        this.project = project;
    }


    @Override
    protected Long doInBackground(Void... voids) {
        long id = dao.insertProject(project);
        return id;
    }
}
