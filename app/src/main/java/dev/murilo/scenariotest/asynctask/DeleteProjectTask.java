package dev.murilo.scenariotest.asynctask;

import android.os.AsyncTask;

import dev.murilo.scenariotest.database.dao.ProjectDAO;
import dev.murilo.scenariotest.model.Project;

public class DeleteProjectTask extends AsyncTask<Void, Void, Void> {

    private final ProjectDAO dao;
    private Project project;

    public DeleteProjectTask(ProjectDAO dao, Project project) {
        this.dao = dao;
        this.project = project;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        dao.remove(project);
        return null;
    }
}
