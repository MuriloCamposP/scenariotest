package dev.murilo.scenariotest.asynctask;

import android.os.AsyncTask;

import java.util.List;

import dev.murilo.scenariotest.database.dao.ProjectRoomDAO;
import dev.murilo.scenariotest.model.ProjectRoom;

public class GetProjectRoomsTask extends AsyncTask<Void, Void, List<ProjectRoom>> {

    private final ProjectRoomDAO dao;
    private final int projectId;

    public GetProjectRoomsTask(ProjectRoomDAO dao, int projectId) {
        this.dao = dao;
        this.projectId = projectId;
    }

    @Override
    protected List<ProjectRoom> doInBackground(Void... voids) {
        return dao.projectRooms(projectId);
    }
}
