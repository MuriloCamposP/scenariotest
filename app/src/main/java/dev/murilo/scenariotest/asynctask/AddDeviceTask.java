package dev.murilo.scenariotest.asynctask;

import android.os.AsyncTask;

import dev.murilo.scenariotest.database.dao.DeviceDAO;
import dev.murilo.scenariotest.model.Device;

public class AddDeviceTask extends AsyncTask<Void, Void, Void> {

    private final DeviceDAO dao;
    private final Device device;

    public AddDeviceTask(DeviceDAO dao, Device device) {
        this.dao = dao;
        this.device = device;
    }


    @Override
    protected Void doInBackground(Void... voids) {
        dao.insertDevice(device);
        return null;
    }
}
