package dev.murilo.scenariotest.asynctask;

import android.os.AsyncTask;

import dev.murilo.scenariotest.database.dao.ProjectRoomDAO;
import dev.murilo.scenariotest.model.ProjectRoom;

public class AddProjectRoomTask extends AsyncTask<Void, Void, Long> {

    private final ProjectRoomDAO dao;
    private ProjectRoom projectRoom;

    public AddProjectRoomTask(ProjectRoomDAO dao, ProjectRoom projectRoom) {
        this.dao = dao;
        this.projectRoom = projectRoom;
    }

    @Override
    protected Long doInBackground(Void... voids) {
        long id = dao.insertProjectRoom(projectRoom);
        return id;
    }
}
