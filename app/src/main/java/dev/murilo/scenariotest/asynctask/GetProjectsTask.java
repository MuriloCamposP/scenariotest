package dev.murilo.scenariotest.asynctask;

import android.os.AsyncTask;

import java.util.List;

import dev.murilo.scenariotest.database.dao.ProjectDAO;
import dev.murilo.scenariotest.model.Project;

public class GetProjectsTask extends AsyncTask<Void, Void, List<Project>> {

    private final ProjectDAO dao;

    public GetProjectsTask(ProjectDAO dao) {
        this.dao = dao;
    }

    @Override
    protected List<Project> doInBackground(Void... voids) {
        return dao.allProjects();
    }
}
