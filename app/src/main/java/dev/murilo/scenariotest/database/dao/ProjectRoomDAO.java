package dev.murilo.scenariotest.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import dev.murilo.scenariotest.model.ProjectRoom;

@Dao
public interface ProjectRoomDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertProjectRoom(ProjectRoom projectRoom);

    @Query("SELECT * FROM ProjectRoom WHERE projectId = :id")
    List<ProjectRoom> projectRooms(int id);

    @Delete
    void remove(ProjectRoom room);
}
