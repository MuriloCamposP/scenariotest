package dev.murilo.scenariotest.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import dev.murilo.scenariotest.database.converter.CalendarConverter;
import dev.murilo.scenariotest.database.converter.ENumScenarioDeviceConverter;
import dev.murilo.scenariotest.database.dao.DeviceDAO;
import dev.murilo.scenariotest.database.dao.ProjectDAO;
import dev.murilo.scenariotest.database.dao.ProjectRoomDAO;
import dev.murilo.scenariotest.model.Device;
import dev.murilo.scenariotest.model.Project;
import dev.murilo.scenariotest.model.ProjectRoom;

@Database(entities = {Project.class, ProjectRoom.class, Device.class}, version = 1, exportSchema = false)
@TypeConverters({CalendarConverter.class, ENumScenarioDeviceConverter.class})
public abstract class ScenarioDatabase extends RoomDatabase {

    private static volatile ScenarioDatabase INSTANCE;
    private static final String DATABASE_NAME = "scenario.db";
    public abstract ProjectDAO getProjectDAO();
    public abstract ProjectRoomDAO getProjectRoomDAO();
    public abstract DeviceDAO getDeviceDAO();

    public static ScenarioDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (ScenarioDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context,
                            ScenarioDatabase.class, DATABASE_NAME)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
