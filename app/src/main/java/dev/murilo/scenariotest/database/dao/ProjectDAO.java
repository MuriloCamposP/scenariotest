package dev.murilo.scenariotest.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import dev.murilo.scenariotest.model.Project;

@Dao
public interface ProjectDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertProject(Project project);

    @Query("SELECT * FROM Project")
    List<Project> allProjects();

    @Delete
    void remove(Project project);
}
