package dev.murilo.scenariotest.database.converter;

import androidx.room.TypeConverter;

import java.util.Calendar;

public class CalendarConverter {
    @TypeConverter
    public Long toLong(Calendar value){
        if(value != null){
            return value.getTimeInMillis();
        }
        return null;
    }

    @TypeConverter
    public Calendar toCalendar(Long value){
        Calendar newDate = Calendar.getInstance();
        if(value != null){
            newDate.setTimeInMillis(value);
        }
        return newDate;
    }
}
