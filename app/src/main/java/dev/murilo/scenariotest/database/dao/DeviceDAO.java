package dev.murilo.scenariotest.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import dev.murilo.scenariotest.model.Device;

@Dao
public interface DeviceDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertDevice(Device newDevice);

    @Query("SELECT * FROM Device WHERE roomId = :id")
    List<Device> roomDevices(int id);

    @Delete
    void remove(Device device);
}
