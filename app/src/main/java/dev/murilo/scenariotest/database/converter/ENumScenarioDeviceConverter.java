package dev.murilo.scenariotest.database.converter;

import androidx.room.TypeConverter;

import dev.murilo.scenariotest.model.ENumScenarioDevice;

public class ENumScenarioDeviceConverter {
    @TypeConverter
    public String convertToString(ENumScenarioDevice kindOfDevice) {
        return kindOfDevice.name();
    }

    @TypeConverter
    public ENumScenarioDevice convertToEnum(String value) {
        if (value != null) {
            return ENumScenarioDevice.valueOf(value);
        }
        return ENumScenarioDevice.MODULO_ILUMINACAO;
    }
}
