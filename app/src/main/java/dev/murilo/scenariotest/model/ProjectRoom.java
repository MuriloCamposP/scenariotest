package dev.murilo.scenariotest.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Project.class,
        parentColumns = "id",
        childColumns = "projectId",
        onUpdate = CASCADE,
        onDelete = CASCADE))
public class ProjectRoom implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String picturePath;
    @ForeignKey(entity = Project.class,
            parentColumns = "id",
            childColumns = "projectId",
            onUpdate = CASCADE,
            onDelete = CASCADE)
    private int projectId;

    public ProjectRoom(){}

    @Ignore
    private ProjectRoom(Parcel p) {
        id = p.readInt();
        name = p.readString();
        picturePath = p.readString();
        projectId = p.readInt();
    }

    public static final Parcelable.Creator<ProjectRoom>
            CREATOR = new Parcelable.Creator<ProjectRoom>() {

        public ProjectRoom createFromParcel(Parcel in) {
            return new ProjectRoom(in);
        }

        public ProjectRoom[] newArray(int size) {
            return new ProjectRoom[size];
        }
    };

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(picturePath);
        dest.writeInt(projectId);
    }
}
