package dev.murilo.scenariotest.model;

public enum ENumScenarioDevice {
    MODULO_ILUMINACAO, TECLADOS, MODULO_CONTROLE_CORTINA
}
