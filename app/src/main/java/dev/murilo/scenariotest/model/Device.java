package dev.murilo.scenariotest.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = ProjectRoom.class,
        parentColumns = "id",
        childColumns = "roomId",
        onUpdate = CASCADE,
        onDelete = CASCADE))
public class Device implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private long id;

    private String name;
    private String deviceManufacturer;
    private ENumScenarioDevice kindOfDevice;
    @ForeignKey(entity = ProjectRoom.class,
            parentColumns = "id",
            childColumns = "roomId",
            onUpdate = CASCADE,
            onDelete = CASCADE)
    private int roomId;

    public Device(){}

    @Ignore
    public Device(String name, ENumScenarioDevice kindOfDevice) {
        this.name = name;
        this.kindOfDevice = kindOfDevice;
    }

    @Ignore
    public Device(String name, String deviceManufacturer) {
        this.name = name;
        this.deviceManufacturer = deviceManufacturer;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeviceManufacturer() {
        return deviceManufacturer;
    }

    public void setDeviceManufacturer(String deviceManufacturer) {
        this.deviceManufacturer = deviceManufacturer;
    }

    public ENumScenarioDevice getKindOfDevice() {
        return kindOfDevice;
    }

    public void setKindOfDevice(ENumScenarioDevice kindOfDevice) {
        this.kindOfDevice = kindOfDevice;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }
}
