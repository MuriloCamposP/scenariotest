# Scenario teste mobile

### Para adicionar um projeto, ambiente ou dispositivos; Basta pressionar o botão de adicionar em cada tela:

![Alt Text] (https://media.giphy.com/media/WQHujaXg4XRmM5sgOK/giphy.gif)
![Alt Text] (https://media.giphy.com/media/L2lbL55pfFmt85G7MR/giphy.gif)
![Alt Text] (https://media.giphy.com/media/U17BGQRbx02KX9MUMw/giphy.gif)

### No menu Info, você pode ver mais informações sobre a Scenario:

![Alt Text] (https://media.giphy.com/media/WmpFHZfm2ehIzdyoIo/giphy.gif)

### Para remover um projeto, ambiente ou dispositivo; Deslize o item que deseja remover para qualquer lado:

![Alt Text] (https://media.giphy.com/media/US2GDGDBQO40ze7LSv/giphy.gif)
